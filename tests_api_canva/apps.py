from django.apps import AppConfig


class TestsApiCanvaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tests_api_canva'
